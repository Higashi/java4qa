package com.idemia.java4qa;

import static java.lang.Integer.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.idemia.java4qa.exercise.Fibonacci;

public class FibonacciTest {

    @Test
    public void testFibonacciArray() {
        assertEquals(valueOf("0"), Fibonacci.number(0), "For number 0 Fibonacci should be 0");
        assertEquals(valueOf("1"), Fibonacci.number(1), "For number 1 Fibonacci should be 1");
        assertEquals(valueOf("1"), Fibonacci.number(2), "For number 2 Fibonacci should be 1");
        assertEquals(valueOf("2"), Fibonacci.number(3), "For number 3 Fibonacci should be 2");
        assertEquals(valueOf("8"), Fibonacci.number(6), "For number 6 Fibonacci should be 8");
        assertEquals(valueOf("21"), Fibonacci.number(8), "For number 8 Fibonacci should be 21");
        assertEquals(valueOf("34"), Fibonacci.number(9), "For number 9 Fibonacci should be 34");
        assertEquals(valueOf("1346269"), Fibonacci.number(31), "For number 31 Fibonacci should be 1346269");
        assertThrows(IllegalArgumentException.class, () -> Fibonacci.number(null));
    }
}
