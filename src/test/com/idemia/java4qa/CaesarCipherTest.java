package com.idemia.java4qa;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.idemia.java4qa.exercise.CaesarCipher;

class CaesarCipherTest {
    private static CaesarCipher cc;

    @BeforeAll
    public static void setup() {
        cc = new CaesarCipher(5);
    }

    @Test
    public void encryptionTest() {
        assertEquals("f", cc.encode("a"), "For shift 5, caesar cipher encoding 'a' should give 'f'");
        assertEquals("e", cc.encode("z"), "For shift 5, caesar cipher encoding 'z' should give 'e'");
        assertThrows(IllegalArgumentException.class, () -> cc.encode(null));
    }

    @Test
    public void decryptionTest() {
        assertEquals("a", cc.decode("f"), "For shift 5, caesar cipher decoding 'f' should give 'a'");
        assertEquals("z", cc.decode("e"), "For shift 5, caesar cipher decoding 'e' should give 'z'");
        assertThrows(IllegalArgumentException.class, () -> cc.decode(null));

    }

    @Test
    public void nullShiftTest() {
        assertThrows(IllegalStateException.class,  () -> new CaesarCipher(null));
    }

}