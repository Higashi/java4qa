package com.idemia.java4qa;

import static com.idemia.java4qa.exercise.Palindrome.findLongest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.idemia.java4qa.exercise.Palindrome;

class PalindromeTest {

    @Test
    void isPalindrome() {
        assertTrue(Palindrome.isPalindrome("aba"));
        assertTrue(Palindrome.isPalindrome("a"));
        assertFalse(Palindrome.isPalindrome(""));
        assertFalse(Palindrome.isPalindrome(null));
        assertFalse(Palindrome.isPalindrome("aab"));
        assertFalse(Palindrome.isPalindrome(" "));
    }

    @Test
    public void findLongestTest() {
        assertEquals("aba", findLongest("aba aba"),
                "Longest palindrome should be 'aba'");
        assertThrows(IllegalArgumentException.class, () -> findLongest(null));
    }

}