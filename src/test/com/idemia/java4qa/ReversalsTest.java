package com.idemia.java4qa;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.idemia.java4qa.exercise.Reversals;

class ReversalsTest {
    private static final String EXPECTED_MESSAGE = "Strings expected to be the same";

    @Test
    public void reverseStringTest() {
        assertEquals(Reversals.reverse("abcd"), "dcba", EXPECTED_MESSAGE);
        assertEquals(Reversals.reverse("a"), "a", EXPECTED_MESSAGE);
        assertEquals(Reversals.reverse(""), "", EXPECTED_MESSAGE);
        assertThrows(IllegalArgumentException.class, () -> Reversals.reverse(null));
    }

}