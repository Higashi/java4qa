package com.idemia.java4qa.exercise;

/**
 * The Caesar cipher is a basic encryption technique used by Julius Caesar to securely communicate with his generals.
 * Each letter is replaced by another letter N positions down the english alphabet.
 * For example, for a rotation of 5, the letter 'c' would be replaced by an 'h'.
 * In case of a 'z', the alphabet rotates and it is transformed into a 'd'.
 *
 * Implements a encoder and decoder for the Ceasar cipher for given number.
 */
public class CaesarCipher {

    private final Integer shift;

    /**
     * @param shift of Caesar cipher.
     */
    public CaesarCipher(final Integer shift) {
        this.shift = shift;
    }

    public String decode(final String encodedMessage) {
        throw new UnsupportedOperationException("Not implemented, yet");
    }

    public String encode(final String plainText) {
        throw new UnsupportedOperationException("Not implemented, yet");
    }
}
