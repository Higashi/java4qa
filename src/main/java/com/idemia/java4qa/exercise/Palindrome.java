package com.idemia.java4qa.exercise;

public class Palindrome {
    private Palindrome() {}

    /**
     * Check if given argument is an palindrome.
     * @return true, if argument is palindrome.
     */
    public static boolean isPalindrome(final String text) {
        throw new UnsupportedOperationException("Not implemented, yet");
    }

    /**
     * Finds and returns the longest palindrome in given string
     * @return longest palindrome
     */
    public static String findLongest(final String text) {
        throw new UnsupportedOperationException("Not implemented, yet");
    }

}
